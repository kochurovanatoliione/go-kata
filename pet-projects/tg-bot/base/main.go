package main

import (
	"flag"
	"gitlab.com/kochurovanatoliione/go-kata/pet-projects/tg-bot/clients/telegram"
	"log"
)

const (
	tgBotHost = "api.telegram.org"
)

func main() {
	tgClient := telegram.New(tgBotHost, mustToken())

	//fetcher = fetcher.New()

	//processor = processor.New()

	//consumer.start(fetcher, processor)
}

func mustToken() string {
	// bot -tg-bot-token 'my token'
	token := flag.String("token-bot-token", "", "token for access to Telegram")

	flag.Parse()

	if *token == "" {
		log.Fatal("token is not specified")
	}
	return *token
}
