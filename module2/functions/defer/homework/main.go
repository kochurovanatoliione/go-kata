package main

import "errors"

// MergeDictsJob задание на слияние нескольких мап
type MergeDictsJob struct {
	Dicts      []map[string]string // вход мапы
	Merged     map[string]string   // финишный мап
	IsFinished bool                // булин завершенности задания
}

// ExecuteMergeDictsJob задание на слияние мап
func ExecuteMergeDictsJob(job *MergeDictsJob) (*MergeDictsJob, error) {
	if len(job.Dicts) < 2 {
		return job, errors.New("at least 2 dictionaries are required")
	}

	for _, dict := range job.Dicts {
		if dict == nil {
			return job, errors.New("nil dictionary")
		}

		for k, v := range dict {
			job.Merged[k] = v
		}
	}

	job.IsFinished = true
	return job, nil
}
