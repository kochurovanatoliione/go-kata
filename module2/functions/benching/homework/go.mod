module benching

go 1.18

require (
	github.com/brianvoe/gofakeit/v6 v6.20.2 // indirect
	github.com/mozillazg/go-unidecode v0.2.0 // indirect
)
