package main

import (
	"fmt"
	"math/rand"
	"testing"

	"github.com/brianvoe/gofakeit/v6"
)

type User struct {
	ID       int64
	Name     string `fake:"{firstname}"`
	Products []Product
}

type Product struct {
	UserID int64
	Name   string `fake:"{sentence:3}"`
}

func main() {
	users := genUsers()
	fmt.Println(users)
	products := genProducts()
	fmt.Println(products)
	users = MapUserProducts(users, products)
	fmt.Println(users)
}

func MapUserProducts(users []User, products []Product) []User {

	for i, user := range users {
		for _, product := range products {
			if product.UserID == user.ID {
				users[i].Products = append(users[i].Products, product)
			}
		}
	}

	return users
}

func genProducts() []Product {
	products := make([]Product, 1000)
	for i, product := range products {
		_ = gofakeit.Struct(&product)
		product.UserID = int64(rand.Intn(100) + 1)
		products[i] = product
	}

	return products
}

func genUsers() []User {
	users := make([]User, 100)
	for i, user := range users {
		_ = gofakeit.Struct(&user)
		user.ID = int64(i)
		users[i] = user
	}

	return users
}

func MapUserProducts2(users []User, products []Product) []User {
	productMap := make(map[int64][]Product, len(users)) // оптимальное выделение памяти мапы продактс

	for _, user := range users {
		productMap[user.ID] = []Product{} // добавление пользователя в мапу с пустым слайсом
	}

	for _, product := range products {
		if productList, ok := productMap[product.UserID]; ok { // проверка наличия юзера в мапе
			productMap[product.UserID] = append(productList, product) // добавление продакт в слайс для юзера
		}
	}

	for i, user := range users {
		users[i].Products = productMap[user.ID] // извлечение продуктс для юзера из мапы
	}

	return users
}

func BenchmarkMapUserProducts(b *testing.B) {
	users := genUsers()
	products := genProducts()
	for i := 0; i < b.N; i++ {
		_ = MapUserProducts(users, products)
	}
}

func BenchmarkMapUserProducts2(b *testing.B) {
	users := genUsers()
	products := genProducts()
	for i := 0; i < b.N; i++ {
		_ = MapUserProducts2(users, products)
	}
}
