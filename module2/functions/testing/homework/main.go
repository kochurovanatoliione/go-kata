package main

import (
	"fmt"
	"regexp"
	"testing"
)

func Greet(name string) string {
	russian := regexp.MustCompile(`^[а-яА-Я]+$`) // проверка строки на ру с помощью regexp
	if russian.MatchString(name) {
		return fmt.Sprintf("Привет %s, добро пожаловать!", name)
	}
	return fmt.Sprintf("Hello %s, you welcome!", name) // иначе енг
}

func TestGreet(t *testing.T) {
	type args struct {
		name string
	}
	tests := []struct {
		name string
		args args
		want string
	}{
		{
			name: "english name",
			args: args{name: "John"},
			want: "Hello John, you welcome!",
		},
		{
			name: "russian name",
			args: args{name: "Боря"},
			want: "Привет Боря, добро пожаловать!",
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := Greet(tt.args.name); got != tt.want {
				t.Errorf("Greet() = %v, want %v", got, tt.want)
			}
		})
	}
}

//func main() {
//	fmt.Println(Greet("Борис"))
//	fmt.Println(Greet("John"))
//}
