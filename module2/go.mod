module gitlab.com/kochurovanatoliione/go-kata/module2

go 1.18

require (
	github.com/brianvoe/gofakeit v3.18.0+incompatible // indirect
	github.com/mozillazg/go-unidecode v0.2.0 // indirect
	golang.org/x/sync v0.1.0 // indirect
)
