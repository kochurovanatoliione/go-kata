package main

import (
	"fmt"
	"runtime"
)

func main() {
	fmt.Println("i can manage")
	go func() {
		fmt.Println("goroutines in Golang!")
		runtime.Gosched() // добавляем внутрь горутины функцию, которая позволит другим горутинам выполниться
	}()
	fmt.Println("and its awesome!")
	runtime.Gosched() // добавляем внутрь функции main еще одну функцию, которая позволит другим горутинам выполниться
}
