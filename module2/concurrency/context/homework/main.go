package main

import (
	"context"
	"fmt"
	"math/rand"
	"sync"
	"time"
)

func joinChannels(ctx context.Context, chs ...<-chan int) chan int {
	mergedCh := make(chan int)
	go func() {
		wg := &sync.WaitGroup{}
		wg.Add(len(chs))
		for _, ch := range chs {
			go func(ch <-chan int, wg *sync.WaitGroup) {
				defer wg.Done()
				for {
					select {
					case id, ok := <-ch:
						if !ok {
							return
						}
						select {
						case mergedCh <- id:
						case <-ctx.Done():
							return
						}
					case <-ctx.Done():
						return
					}
				}
			}(ch, wg)
		}
		wg.Wait()
		close(mergedCh)
	}()
	return mergedCh
}

func generateData(ctx context.Context) chan int {
	out := make(chan int, 1000)
	go func() {
		defer close(out)
		for {
			select {
			case _, ok := <-out:
				if !ok {
					return
				}
			case <-ctx.Done():
				return
			case out <- rand.Intn(100):
			}
		}
	}()
	return out
}

func main() {
	rand.Seed(time.Now().UnixNano())
	ctx, cancel := context.WithTimeout(context.Background(), 30*time.Second)
	defer cancel()

	a := generateData(ctx)
	b := generateData(ctx)
	c := generateData(ctx)

	mainChan := joinChannels(ctx, a, b, c)
	for {
		select {
		case num, ok := <-mainChan:
			if !ok {
				return
			}
			fmt.Println(num)
		case <-ctx.Done():
			return
		}
	}
}
