package main

import (
	"bytes"
	"fmt"
	"io"
	"io/ioutil"
	"os"
)

func main() {
	// Создаем буфер
	buffer := bytes.Buffer{}
	data := []string{
		"there is 3pm, but im still alive to write this code snippet",
		"чистый код лучше, чем заумный код",
		"ваш код станет наследием будущих программистов",
		"задумайтесь об этом",
	}
	for _, str := range data {
		// Пишем в буффер
		buffer.WriteString(str + "\n")
	}

	// Создаем файл
	file, err := os.Create("example.txt")
	if err != nil {
		panic(err)
	}
	defer file.Close()

	// Копируем данные из буфера в файл
	_, err = io.Copy(file, &buffer)
	if err != nil {
		panic(err)
	}

	// Читаем данные файла в новый буфер
	fileData, err := ioutil.ReadFile("example.txt")
	if err != nil {
		panic(err)
	}
	newBuffer := bytes.NewBuffer(fileData)

	// Выводим данные из нового буфера
	fmt.Println(newBuffer.String())
}
