package main

import (
	"fmt"
	"github.com/brianvoe/gofakeit"
)

func generateSelfStory(name string, age int, money float64) string {
	return fmt.Sprintf("Hello, my name is %s. I'm %d y.o. And I also have $ %.2f in my wallet right now.", name, age, money)
}

func main() {
	name := gofakeit.Name()
	age := gofakeit.Number(18, 90)
	money := gofakeit.Float64Range(0, 1000)

	myStory := generateSelfStory(name, age, money)
	fmt.Println(myStory)
}

// Вывод:
// Hello, my name is Kattie Torphy. I'm 70 y.o. And I also have $ 276.28 in my wallet right now.
