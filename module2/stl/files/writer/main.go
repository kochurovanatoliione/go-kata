package main

import (
	"bufio"
	"fmt"
	"os"
	"unicode"
)

func main() {
	// Записываем данные из os.Stdin в файл
	file, err := os.Create("example.txt")
	if err != nil {
		panic(err)
	}
	defer file.Close()

	writer := bufio.NewWriter(file)

	reader := bufio.NewReader(os.Stdin)

	for {
		fmt.Print("Enter text: ")

		// Читаем строку из os.Stdin
		line, err := reader.ReadString('\n')
		if err != nil {
			panic(err)
		}

		// Если строка пустая, выходим
		if line == "\n" {
			break
		}

		// Записываем строку в файл
		_, err = writer.WriteString(line)
		if err != nil {
			panic(err)
		}
	}

	writer.Flush()

	// Открываем файл для чтения
	inputFile, err := os.Open("example.txt")
	if err != nil {
		panic(err)
	}
	defer inputFile.Close()

	// Создаем файл для записи
	outputFile, err := os.Create("example.processed.txt")
	if err != nil {
		panic(err)
	}
	defer outputFile.Close()

	writer = bufio.NewWriter(outputFile)

	reader = bufio.NewReader(inputFile)

	for {
		// Читаем строку из файла
		line, err := reader.ReadString('\n')
		if err != nil {
			break
		}

		// Преобразуем русские символы в транслит
		translatedLine := make([]rune, len(line))
		for i, ch := range line {
			switch {
			case ch == 'Ё':
				translatedLine[i] = 'E'
			case ch == 'ё':
				translatedLine[i] = 'e'
			case unicode.Is(unicode.Cyrillic, ch):
				// Транслитерация русских символов
				translation, ok := translit[ch]
				if !ok {
					// Если символ не найден, заменяем его на "_"
					translatedLine[i] = '_'
				} else {
					translatedLine[i] = translation
				}
			default:
				translatedLine[i] = ch
			}
		}

		// Записываем строку с транслитерированными символами в файл
		_, err = writer.WriteString(string(translatedLine))
		if err != nil {
			panic(err)
		}
	}

	writer.Flush()
}

// Map для транслитерации русских символов
var translit = map[rune]rune{
	'A':      'А',
	'B':      'Б',
	'V':      'В',
	'G':      'Г',
	'D':      'Д',
	'E':      'Е',
	'\u0416': 'Ж',
	'Z':      'З',
	'I':      'И',
	'K':      'К',
	'L':      'Л',
	'M':      'М',
	'N':      'Н',
	'O':      'О',
	'P':      'П',
	'R':      'Р',
	'S':      'С',
	'T':      'Т',
	'Y':      'У',
	'F':      'Ф',
	'\u0425': 'Х',
	'\u0426': 'Ц',
	'\u0427': 'Ч',
	'\u0428': 'Ш',
	'\u0429': 'Щ',
	'U':      'Ы',
	'Э':      'E',
	'Ю':      '\u042E',
	'Я':      '\u042F',
	'a':      'а',
	'b':      'б',
	'v':      'в',
	'g':      'г',
	'd':      'д',
	'e':      'е',
	'\u0436': 'ж',
	'z':      'з',
	'i':      'и',
	'k':      'к',
	'l':      'л',
	'm':      'м',
	'n':      'н',
	'o':      'о',
	'p':      'п',
	'r':      'р',
	's':      'с',
	't':      'т',
	'u':      'у',
	'f':      'ф',
	'\u0445': 'х',
	'\u0446': 'ц',
	'\u0447': 'ч',
	'\u0448': 'ш',
	'\u0449': 'щ',
	'y':      'ы',
	'\u044E': 'ю',
	'\u044F': 'я',
}
