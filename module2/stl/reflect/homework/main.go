package main

import (
	"fmt"
	"reflect"
	"time"
)

var reflectedStructs map[string][]Field
var tagList = []string{"json", "db", "db_index", "db_type", "db_default", "db_ops"}

type Field struct {
	Name string
	Tags map[string]string
}

type EmailVerifyDTO struct {
	Email string `json:"email" db:"email" db_type:"varchar(89)" db_default:"default null" db_index:"index,unique" db_ops:"create,update"`
}

type UserDTO struct {
	ID            int       `json:"id" db:"id" db_type:"BIGSERIAL primary key" db_default:"not null"`
	Name          string    `json:"name" db:"name" db_type:"varchar(55)" db_default:"default null" db_ops:"create,update"`
	Phone         string    `json:"phone" db:"phone" db_type:"varchar(34)" db_default:"default null" db_index:"index,unique" db_ops:"create,update"`
	Email         string    `json:"email" db:"email" db_type:"varchar(89)" db_default:"default null" db_index:"index,unique" db_ops:"create,update"`
	Password      string    `json:"password" db:"password" db_type:"varchar(144)" db_default:"default null" db_ops:"create,update"`
	Status        int       `json:"status" db:"status" db_type:"int" db_default:"default 0" db_ops:"create,update"`
	Role          int       `json:"role" db:"role" db_type:"int" db_default:"not null" db_ops:"create,update"`
	Verified      bool      `json:"verified" db:"verified" db_type:"boolean" db_default:"not null" db_ops:"create,update"`
	EmailVerified bool      `json:"email_verified" db:"email_verified" db_type:"boolean" db_default:"not null" db_ops:"create,update"`
	PhoneVerified bool      `json:"phone_verified" db:"phone_verified" db_type:"boolean" db_default:"not null" db_ops:"create,update"`
	CreatedAt     time.Time `json:"created_at" db:"created_at" db_type:"timestamp" db_default:"default (now()) not null" db_index:"index"`
	UpdatedAt     time.Time `json:"updated_at" db:"updated_at" db_type:"timestamp" db_default:"default (now()) not null" db_index:"index"`
	DeletedAt     time.Time `json:"deleted_at" db:"deleted_at" db_type:"timestamp" db_default:"default null" db_index:"index"`
}

func populateReflectedStructs() {
	reflectedStructs = make(map[string][]Field)
	var userDTOFields []Field
	var emailVerifyDTOFields []Field

	getType := reflect.TypeOf(UserDTO{})
	for i := 0; i < getType.NumField(); i++ {
		field := getType.Field(i)

		tags := make(map[string]string)
		for _, tag := range tagList {
			val, ok := field.Tag.Lookup(tag)
			if ok {
				tags[tag] = val
			}
		}

		userDTOFields = append(userDTOFields, Field{
			Name: field.Name,
			Tags: tags,
		})
	}

	getType = reflect.TypeOf(EmailVerifyDTO{})
	for i := 0; i < getType.NumField(); i++ {
		field := getType.Field(i)

		tags := make(map[string]string)
		for _, tag := range tagList {
			val, ok := field.Tag.Lookup(tag)
			if ok {
				tags[tag] = val
			}
		}

		emailVerifyDTOFields = append(emailVerifyDTOFields, Field{
			Name: field.Name,
			Tags: tags,
		})
	}

	reflectedStructs["UserDTO"] = userDTOFields
	reflectedStructs["EmailVerifyDTO"] = emailVerifyDTOFields
}

func main() {
	populateReflectedStructs()
	fmt.Println(reflectedStructs["UserDTO"])
	fmt.Println(reflectedStructs["EmailVerifyDTO"])
}
