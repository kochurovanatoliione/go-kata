package main

import (
	"encoding/json"
	"flag"
	"fmt"
	"io/ioutil"
	"log"
)

type Config struct {
	AppName    string `json:"AppName"`
	Production bool   `json:"Production"`
}

func main() {
	// Определение флага для пути к файлу конфигурации
	configPath := flag.String("conf", "go-kata/module2/stl/flag/homework/config.json", "path to config file")
	//configPath := flag.String("conf", "./config.json", "path to config file")
	flag.Parse()

	// Чтение файла конфигурации и декодирование в структуру
	fileData, err := ioutil.ReadFile(*configPath)
	if err != nil {
		log.Fatalf("error reading config file: %v", err)
	}

	conf := &Config{}
	if err := json.Unmarshal(fileData, conf); err != nil {
		log.Fatalf("error unmarshaling config file: %v", err)
	}

	// Вывод настроек конфигурации
	fmt.Printf("Production: %v\nAppName: %v\n", conf.Production, conf.AppName)
}
