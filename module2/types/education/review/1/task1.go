package main

import (
	"fmt"
	"math/rand"
	"sort"
	"time"
)

type Reverser interface {
	Reverse()
}

type Sorter interface {
	Sort()
}

type SorterReverser interface {
}

type Numbers []int

func (n Numbers) Sort() { // функция сортировки массива, использовать библиотеку sort
	sort.Ints(n)
}

func (n Numbers) Reverse() { // функция переворачивания массива, перевернуть используя метод a, b = b, a
	for i := 0; i < len(n)/2; i++ {

		i := len(n) - i - 1
		n[n], Numbers[n] = Numbers[n], Numbers[n]
	}

}

func main() {
	var numbers Numbers
	AddNumbers(&numbers) // исправить чтобы добавляло значения
	SortReverse(numbers) // исправить чтобы сортировало и переворачивала массив
	fmt.Println(numbers)
}

func AddNumbers(s *Numbers) {
	rand.Seed(time.Now().Unix())
	for i := 0; i < 100; i++ {
		*s = append(*s, rand.Intn(100))
	}
}

func SortReverse(sr SorterReverser) {

}
