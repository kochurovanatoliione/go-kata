package main

import (
	"fmt"
)

const (
	PetTypeCat = iota
	PetTypeDog
)

type Pet struct {
	Name  string
	Month int
	Type  int
}

type User struct {
	Name string
	Age  int
}

func main() {
	humanPets := [][]string{
		{"dog", "Alma", "8"},
		{"cat", "Kitty", "1"},
		{"cat", "Kuzya", "5"},
		{"user", "13", "Rob", "Pike"},
		{"user", "55", "Eli", "Bendersky"},
		{"user", "34", "Brad", "Fitzpatrick"},
		{"dog", "Cooper", "13"},
		{"cat", "Milo", "2"},
		{"cat", "Max", ".;№%"},
		{"dog", "Charlie", "8"},
		{"dog", "Dingo", "13"},
		{"dog", "Jerry", "2"},
		{"cat", "Lucky", "5"},
		{"user", "21", "Dave", "Cheney"},
		{"user", "%@#", "Petr", "Filippov"},
	}

	var objects []interface{}
	var (
		age     int
		err     error
		petType int
	)
	for i := range humanPets {
		if humanPets[i][0] == "" { // изменить условие чтобы определить пользователя
			age = 0         // вычислить возраст используя strconv.Atoi
			if err != nil { // если ошибка пропускаем пользователя

			}
			objects = append(objects, User{
				Name: "", // заполнить имя пользователя, объединив имя, фамилию через пробел
				Age:  age,
			})
		} else {
			age = 0         // вычислить возраст используя strconv.Atoi
			if err != nil { // если ошибка пропускаем питомца

			}
			petType = PetTypeDog
			objects = append(objects, Pet{
				Name:  "", // заполнить имя
				Month: age,
				Type:  petType,
			})
		}
	}
	users, pets := groupObjects(objects)
	fmt.Println("users", users)
	fmt.Println("pets", pets)
	fmt.Println("pets per human", petsHumanRatio(len(pets), len(users)))
}

func groupObjects(v []interface{}) ([]User, []Pet) {
	var (
		users []User
		pets  []Pet
	)
	for i := range v {
		switch v[i].(type) {
		case User:
			users = append(users, v[i]) // добавить пользователя в users через type assertion
		case Pet:
			// добавить питомца в pets через type assertion
		}
	}
	sortByAge(users)
	sortByAge(pets)

	return users, pets
}

func petsHumanRatio(petsCount, humanCount int) float64 {
	return 0 // вычислить соотношение пользователей и питомцев используя type casting
}

func sortByAge(v interface{}) {
	switch objects := v.(type) {
	case []User:
		// сортировка по возрасту user.Age используя функцию sort.Slice не используя type assertion, objects уже имеет тип []User
	case []Pet:
		// сортировка по количеству месяцев pet.Month используя функцию sort.Slice не используя type assertion, objects уже имеет тип []Pet
	}
}
