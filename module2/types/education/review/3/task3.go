package main

import (
	"math/rand"
	"time"
)

func init() {
	rand.Seed(time.Now().UnixNano())
}

// User создай структуру User.

// generateAge сгенерируй возраст от 18 до 70 лет.
func generateAge() int {
	// используй rand.Intn()
	return 0
}

// generateIncome сгенерируй доход от 0 до 500000.
func generateIncome() int {
	// используй rand.Intn()
	return 0
}

// generateFullName сгенерируй полное имя. например "John Doe".
func generateFullName() string {
	// создай слайс с именами и слайс с фамилиями.
	// используй rand.Intn() для выбора случайного имени и фамилии.

	return "СлучайноеИмя СлучайнаяФамилия"
}

func main() {
	// Сгенерируй 1000 пользователей и заполни ими слайс users.

	// Выведи средний возраст пользователей.

	// Выведи средний доход пользователей.

	// Выведи количество пользователей, чей доход превышает средний доход.

}
