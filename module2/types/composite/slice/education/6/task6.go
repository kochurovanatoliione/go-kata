//Напишите функцию, которая проверяет, содержит ли слайс хотя бы один элемент, удовлетворяющий заданному условию.
package main

import "fmt"

func main() {
	nums := []int{1, 2, 3, 4, 5}
	containsEven := contains(nums, func(n int) bool {
		return n%2 == 0
	})
	fmt.Println(containsEven) // вывод true
}

func contains(nums []int, condition func(int) bool) bool {
	for _, num := range nums {
		if condition(num) {
			return true
		}
	}
	return false
}
