//Напишите функцию, которая объединяет два слайса целых чисел и удаляет дубликаты.
package main

import "fmt"

func main() {
	nums1 := []int{1, 2, 3, 4, 5}

	nums2 := []int{3, 4, 5, 6, 7}

	result := mergeAndRemoveDuplicates(nums1, nums2)

	fmt.Println(result) // вывод [1 2 3 4 5 6 7]
}

func mergeAndRemoveDuplicates(nums1 []int, nums2 []int) []int {

	// объединяем слайсы

	nums := append(nums1, nums2...)
	//fmt.Println(nums) //два слайса объединены в один без удаления дубликатов

	// удаляем дубликаты

	encountered := map[int]bool{}

	result := []int{}

	for _, num := range nums {

		if encountered[num] == false {

			encountered[num] = true

			result = append(result, num)

		}

	}

	return result

}
