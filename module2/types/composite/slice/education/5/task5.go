// Напишите функцию, которая возвращает новый слайс, состоящий только из четных чисел из исходного слайса.
package main

import "fmt"

func main() {
	nums := []int{1, 2, 3, 4, 5, 6, 7, 8, 9, 10}

	result := filterEven(nums)

	fmt.Println(result) // вывод [2 4 6 8 10]
}

func filterEven(nums []int) []int {

	result := []int{}

	for _, num := range nums {

		if num%2 == 0 {

			result = append(result, num)

		}

	}

	return result

}
