//Напишите функцию, которая удаляет дубликаты из слайса строк.

package main

import "fmt"

func main() {
	strs := []string{"test", "test", "test1", "test33", "test33"} //создаем слайс из рандомных одинаковых и разных слов

	fmt.Println(removeDuplicates(strs)) // выводим слайс strs из ф-ции removeDuplicates

}

func removeDuplicates(strs []string) []string { // cоздаем массив который принимает слайс строк и отдает слайс строк

	encountered := map[string]bool{} // создаем мапу ключей стринг и значений булин

	result := []string{} // создаем result как пустой слайс стрингов

	for _, str := range strs { // запускаем цикл range где str = range strs

		if encountered[str] == false { // если мапа encountered с ключем str равно false

			encountered[str] = true // то мапа encountered с ключем str присваивается true

			result = append(result, str) // выводим result который = добавлением в пустой result str, тем самым наполнив
			// пустой слайс result только значениями range str=true

		}

	}

	return result

}
