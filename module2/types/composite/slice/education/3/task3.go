// Напишите функцию, которая сортирует слайс строк в порядке возрастания
package main

import (
	"fmt"
	"sort"
)

func main() {

	strings := []string{"pear", "banana", "apple", "orange", "kiwi", "melon"}
	result := sortStrings(strings)
	fmt.Println(result) // вывод ["apple" "banana" "kiwi" "melon" "orange" "pear"]
}

func sortStrings(strs []string) []string {
	sort.Strings(strs)
	return strs
}
