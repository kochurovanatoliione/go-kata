// Напишите функцию, которая находит максимальный элемент в слайсе целых чисел.

package main

import "fmt"

func main() {
	nums := []int{456, 2, 45, 74, 2341234} // инициализируем слайс рандомных цифр

	fmt.Println(findMax(nums)) //выводим переданный слайс nums в ф-цию findMax
}

func findMax(nums []int) int { // создаем ф-цию, которая на выход принимает слайс int и выводит int

	if len(nums) == 0 { // проверяем на пустой nums измеряя длину слайса

		return 0 // если длина слайса равна 0, то вернуть 0
	}

	var max int = nums[0] // создаем переменную max, которая равна 0 числу слайса nums

	for _, num := range nums { // запускаем цикл range где num это range nums

		if num > max {

			max = num // изменяем max на num если num > max в цикле
		}

	}

	return max
}
