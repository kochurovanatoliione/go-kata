//Напишите функцию, которая удаляет из слайса все элементы, не удовлетворяющие заданному условию.
package main

import "fmt"

func main() {
	numbers := []int{1, 2, 3, 4, 5, 6, 7, 8, 9}
	res := filter(numbers, func(num int) bool {
		return num%2 == 0
	})
	fmt.Println(res)
	// Output: [2 4 6 8]
}
func filter(nums []int, condition func(int) bool) []int {
	result := []int{}
	for _, num := range nums {
		if condition(num) {
			result = append(result, num)
		}
	}
	return result
}
