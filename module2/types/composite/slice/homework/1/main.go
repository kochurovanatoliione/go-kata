package main

import "fmt"

func main() {
	s := []int{1, 2, 3}
	Append(&s)     // передаем слайс по ссылке
	fmt.Println(s) // [1 2 3 4]
}

func Append(s *[]int) { // принимаем слайс по ссылке
	*s = append(*s, 4) // добавляем 4-ый элемент в слайс s
}
