// You can edit this code!
// Click here and start typing.
package main

import "fmt"

type Project struct {
	Name  string
	Stars int
}

func main() {
	projects := []Project{
		{
			Name:  "https://github.com/docker/compose",
			Stars: 27600,
		},
		{
			Name:  "https://github.com/map/compose",
			Stars: 2700,
		},
		{
			Name:  "https://github.com/mapompose",
			Stars: 27020,
		},
		{
			Name:  "https://github.com/map",
			Stars: 700,
		},
		{
			Name:  "https://github.com/compose",
			Stars: 67700,
		},
		{
			Name:  "https://github.com/mase",
			Stars: 7116,
		},
		{
			Name:  "https://github.com/map/cse",
			Stars: 45678,
		},
		{
			Name:  "https://github.com/slise",
			Stars: 9708,
		},
		{
			Name:  "https://github.com/map/e",
			Stars: 9707,
		},
		{
			Name:  "https://github.com/mpose",
			Stars: 9799,
		},
		{
			Name:  "https://github.com/maose",
			Stars: 9000,
		},
		{
			Name:  "https://github.com/mcompose",
			Stars: 29817,
		},
		{
			Name:  "https://github.com/macoo",
			Stars: 27678,
		},
		// сюда впишите ваши остальные 12 структур
	}

	m := map[string]Project{}

	// в цикле запишите в map
	for i := range projects {
		m[projects[i].Name] = projects[i]
	}

	// в цикле пройдитесь по мапе и выведите значения в консоль
	for i := range m {
		fmt.Println(m[i])
	}

}
