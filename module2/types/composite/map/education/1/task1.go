//Напишите функцию, которая считает количество вхождений каждого элемента в слайс и возвращает результат
//в виде map[string]int, где ключ - это элемент, а значение - количество его вхождений.
package main

import "fmt"

func main() {
	strings := []string{"apple", "banana", "orange", "apple", "kiwi", "orange", "melon"} // cоздаем слайс из стрингов
	result := countOccurrences(strings)                                                  // создаем переменную которая передает strings в ф-цию countOccurrences
	fmt.Println(result)                                                                  // вывод map[apple:2 banana:1 kiwi:1 melon:1 orange:2]
}

func countOccurrences(strs []string) map[string]int { // cоздаем ф-цию которая принимает слайс стринг и отдает мапу
	counts := make(map[string]int) // создаем мапу с ключем стринг и значением инт

	for _, str := range strs { // запускаем цикл range где str = range strs
		counts[str]++ // в цикле записываем в мапу counts с ключем str инкремент значения
	}

	return counts // возвращаем мапу counts которая наполнилась в цикле ключей str и значений инкремент
}
