//Напишите функцию, которая возвращает новый map, содержащий только те элементы,
//ключи которых удовлетворяют заданному условию.
package main

import "fmt"

func main() {
	m := map[string]int{
		"a": 1,
		"b": 2,
		"c": 3,
		"d": 4,
	}

	condition := func(key string) bool {
		return key != "a" && key != "c"
	}

	filteredMap := filterByCondition(m, condition)
	fmt.Println(filteredMap) // Output: map[b:2 d:4]
}

func filterByCondition(m map[string]int, condition func(key string) bool) map[string]int {
	result := make(map[string]int)
	for k, v := range m {
		if condition(k) {
			result[k] = v
		}
	}
	return result
}
