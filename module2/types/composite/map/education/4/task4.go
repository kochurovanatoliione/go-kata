//Напишите функцию, которая проверяет, содержит ли map значение, удовлетворяющее заданному условию.
package main

import "fmt"

func main() {
	m := map[string]int{
		"a": 1,
		"b": 2,
		"c": 3,
		"d": 4,
	}

	condition := func(value int) bool {
		return value > 5
	}

	containsValue := containsValueByCondition(m, condition)
	fmt.Println(containsValue) // Output false
}

func containsValueByCondition(m map[string]int, condition func(value int) bool) bool {
	for _, v := range m {
		if condition(v) {
			return true
		}
	}
	return false
}
