//Напишите функцию, которая объединяет два map[string]int и возвращает новый map, содержащий значения
//из обоих исходных map, если ключи не дублируются.
//Если ключи дублируются, то значение должно быть суммой значений для каждого ключа.
package main

import "fmt"

func main() {
	map1 := map[string]int{"apple": 2, "banana": 3, "kiwi": 1}   // cоздаем мапу map1 с ключами, значениями
	map2 := map[string]int{"banana": 5, "melon": 2, "orange": 4} // cоздаем мапу map2 с некоторыми кючами из map1
	result := mergeMaps(map1, map2)                              // создаем result которая передает map1, map2 в ф-цию mergeMaps
	fmt.Println(result)                                          // вывод map[apple:2 banana:8 kiwi:1 melon:2 orange:4]
}

func mergeMaps(map1, map2 map[string]int) map[string]int {

	result := make(map[string]int)

	for key := range map1 {

		result[key] += map1[key]

	}

	for key := range map2 {

		result[key] += map2[key]

	}

	return result

}
