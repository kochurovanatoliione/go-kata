//Напишите функцию, которая находит среднее арифметическое значений
//в заданном массиве float64 с помощью указателей.
package main

import "fmt"

func main() {
	arr := []float64{3.5, 5.0, 2.2, 7.8, 4.1}
	avg := findAverage(&arr)     // инициализируем переменную avg которая передает &arr в findAverage
	fmt.Println("Array:", arr)   // вывод всего массива arr
	fmt.Println("Average:", avg) // вывод avg
}

func findAverage(arr *[]float64) float64 {
	n := len(*arr)           // инициализируем переменную n которая равна длине *arr
	sum := 0.0               // инициализируем sum которая равна 0.0
	for _, v := range *arr { // проходим циклом range массив *arr
		sum += v // прибавляем каждый цикл v к sum, чтобы получить сумму всего массива
	}
	return sum / float64(n) // возвращаем sum деленое на n(длину *arr)
}
