//Напишите функцию, которая принимает указатель на int и возвращает указатель на его квадрат.
package main

import "fmt"

//func main() {
//	x := 5
//	fmt.Println("Original value:", x) // вывод переменной х до квадрата x
//
//	result := squarePtr(&x)                // передача с помощью инициализации result в ф-цию squarePtr адреса на переменную х
//	fmt.Println("Squared value:", *result) // вывод указателя на result
//}

//func squarePtr(p *int) *int { // ф-ция получает на вход указатель на значение int и возвращает int
//	result := (*p) * (*p) // инициализируем result, который является умножением *р само на себя
//	return &result        // возвращаем адрес на result
//}

func pQuadr(p *int) *int {
	result := (*p) * (*p)
	return &result
}

func main() {
	x := 10
	result := pQuadr(&x)
	fmt.Println(*result)
}
