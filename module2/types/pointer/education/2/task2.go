//Напишите функцию, которая находит минимальное и максимальное
//значения в заданном массиве int с помощью указателей.
package main

import "fmt"

func main() {
	arr := []int{3, 5, 2, 7, 4}

	min, max := findMinMax(&arr) // инициализируем и передаем min, max, a так же адрес на arr в findMinMax
	fmt.Println("Array:", arr)
	fmt.Println("Min:", min)
	fmt.Println("Max:", max)
}

func findMinMax(arr *[]int) (int, int) {
	min := (*arr)[0] //инициализируем min и назначаем его как [0] ссылки на arr
	max := (*arr)[0] //инициализируем max и назначаем его как [0] ссылки на arr

	for _, v := range *arr { //проходим циклом range *arr
		if v < min {
			min = v // если v меньше min, то min = v
		}
		if v > max {
			max = v // если v > max, то max = v
		}
	}

	return min, max
}
