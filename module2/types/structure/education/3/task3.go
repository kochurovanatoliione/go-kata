//Напишите структуру Person, которая содержит поля name и age. Напишите метод, который печатает информацию
//о человеке в формате "Имя: {name}, Возраст: {age}".
package main

import "fmt"

func main() {
	p := Person{name: "Иван", age: 35}
	p.PrintInfo() // Имя: Иван, Возраст: 35
}

type Person struct {
	name string
	age  int
}

func (p Person) PrintInfo() {
	fmt.Printf("Имя: %s, Возраст: %d\n", p.name, p.age)
}
