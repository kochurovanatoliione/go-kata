//Напишите структуру Student, которая содержит поля name, age и grade. Напишите метод, который печатает
//информацию о студенте в формате "Имя: {name}, Возраст: {age}, Класс: {grade}".
package main

import "fmt"

func main() {
	s := Student{name: "Анна", age: 16, grade: 10}
	s.PrintInfo() // Имя: Анна, Возраст: 16, Класс: 10
}

type Student struct {
	name  string
	age   int
	grade int
}

func (s Student) PrintInfo() {
	fmt.Printf("Имя: %s, Возраст: %d, Класс: %d\n", s.name, s.age, s.grade)
}
