//Напишите структуру Rectangle, которая содержит поля width и height.
//Напишите методы для вычисления периметра и площади прямоугольника.
package main

import "fmt"

func main() {
	r := Rectangle{width: 10, height: 5}
	fmt.Println("Area:", r.Area())           // Area: 50
	fmt.Println("Perimeter:", r.Perimeter()) // Perimeter: 30
}

type Rectangle struct {
	width  float64
	height float64
}

func (r Rectangle) Area() float64 {
	return r.width * r.height
}

func (r Rectangle) Perimeter() float64 {
	return 2*r.width + 2*r.height
}
