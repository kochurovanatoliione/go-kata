// Напишите структуру Circle, которая содержит поле radius.
//Напишите методы для вычисления диаметра, длины окружности и площади круга.

package main

import (
	"fmt"
	"math"
)

func main() {
	c := Circle{radius: 5}
	fmt.Println("Diameter:", c.Diameter())           // Diameter: 10
	fmt.Println("Circumference:", c.Circumference()) // Circumference: 31.41592653589793
	fmt.Println("Area:", c.Area())                   // Area: 78.53981633974483
}

type Circle struct {
	radius float64
}

func (c Circle) Diameter() float64 {
	return c.radius * 2
}

func (c Circle) Circumference() float64 {
	return 2 * math.Pi * c.radius
}

func (c Circle) Area() float64 {
	return math.Pi * math.Pow(c.radius, 2)
}
