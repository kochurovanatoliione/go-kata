//Напишите функцию, которая принимает на вход любой тип данных и выводит его значения в строковом формате.
package main

import "fmt"

func main() {
	a := 42
	b := "hello"
	c := true

	fmt.Println(printValueAsString(a))
	fmt.Println(printValueAsString(b))
	fmt.Println(printValueAsString(c))
}

func printValueAsString(value interface{}) string {
	return fmt.Sprintf("%v", value)
}
