// Напишите функцию, которая принимает на вход любой тип данных и возвращает true,
//если это строка, и false в противном случае.
package main

import (
	"fmt"
)

//func main() {
//	var x1 int = 42
//	var x2 string = "hello"
//	var x3 bool = true
//
//	fmt.Println(isString(x1)) // Output: false
//	fmt.Println(isString(x2)) // Output: true
//	fmt.Println(isString(x3)) // Output: false
//}

func main() {

	var x1 = "hello"
	var x2 = true

	fmt.Println(typeTrue(x1))
	fmt.Println(typeTrue(x2))
}

func typeTrue(x interface{}) bool {
	_, ok := x.(string)

	return ok
}
