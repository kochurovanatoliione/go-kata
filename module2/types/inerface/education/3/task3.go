//Напишите интерфейс Shape, который определяет методы для вычисления площади и периметра фигуры.
package main

import (
	"fmt"
	"math"
)

func main() {
	var shape Shape

	shape = Circle{radius: 5}
	fmt.Println("Circle area:", shape.Area())
	fmt.Println("Circle perimeter:", shape.Perimeter())

	shape = Square{side: 4}
	fmt.Println("Square area:", shape.Area())
	fmt.Println("Square perimeter:", shape.Perimeter())
}

type Shape interface {
	Area() float64
	Perimeter() float64
}

type Circle struct {
	radius float64
}

type Square struct {
	side float64
}

func (c Circle) Area() float64 {
	return math.Pi * c.radius * c.radius
}

func (c Circle) Perimeter() float64 {
	return 2 * math.Pi * c.radius
}

func (s Square) Area() float64 {
	return s.side * s.side
}

func (s Square) Perimeter() float64 {
	return 4 * s.side
}
