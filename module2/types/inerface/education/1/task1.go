//Напишите функцию, которая принимает на вход любой тип данных и выводит его тип.
package main

import "fmt"

func main() {

	x := "hello"
	y := 5
	z := 3.14

	typePrint(x)
	typePrint(y)
	typePrint(z)

}

func typePrint(x interface{}) {
	fmt.Printf("%T\n", x)
}
