// You can edit this code!
// Click here and start typing.
package main

import (
	"fmt"
)

type MyInterface interface{}

func main() {
	var n *int
	fmt.Println(n == nil)
	test(n)
}

func test(r interface{}) {
	switch r.(type) {
	case nil:
		fmt.Println("Failure!")
	default:
		fmt.Println("Success!")
	}
}
