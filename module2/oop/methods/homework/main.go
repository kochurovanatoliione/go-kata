package main

import (
	"math"
	"testing"
)

type calc struct {
	a, b   float64
	result float64
}

func NewCalc() *calc {
	return &calc{}
}

func (c *calc) SetA(a float64) *calc {
	c.a = a
	return c
}

func (c *calc) SetB(b float64) *calc {
	c.b = b
	return c
}

func (c *calc) Do(operation func(a, b float64) float64) *calc {
	c.result = operation(c.a, c.b)
	return c
}

func (c calc) Result() float64 {
	return c.result
}

func (c *calc) Reset() *calc {
	c.a, c.b, c.result = 0, 0, 0
	return c
}

func divide(a, b float64) float64 {
	return a / b
}

func sum(a, b float64) float64 {
	return a + b
}

func average(a, b float64) float64 {
	return (a + b) / 2
}

func subtract(a, b float64) float64 {
	return a - b
}

func multiply(a, b float64) float64 {
	return a * b
}

func TestCalc(t *testing.T) {
	testCases := []struct {
		name string
		a, b float64
		want float64
		op   func(a, b float64) float64
	}{
		{"Add zero to zero", 0, 0, 0, sum},
		{"Add zero to negative", 0, -3, -3, sum},
		{"Add negative to positive", 3, -5, -2, sum},
		{"Add positive to positive", 2, 2, 4, sum},
		{"Subtract zero from zero", 0, 0, 0, subtract},
		{"Subtract zero from negative", 0, -3, 3, subtract},
		{"Subtract negative from positive", 3, -5, 8, subtract},
		{"Subtract positive from positive", 2, 2, 0, subtract},
		{"Multiply negative by zero", -7, 0, 0, multiply},
		{"Multiply positive by negative", 2, -4, -8, multiply},
		{"Multiply positive by positive", 4, 6, 24, multiply},
		{"Divide by zero", 5, 0, math.Inf(0), divide},
		{"Divide by positive number", 6, 2, 3, divide},
		{"Calculate average of negative numbers", -2, -4, -3, average},
		{"Calculate average of positive numbers", 6, 10, 8, average},
	}
	for _, tt := range testCases {
		t.Run(tt.name, func(t *testing.T) {
			c := NewCalc().SetA(tt.a).SetB(tt.b)
			got := c.Do(tt.op).Result()
			if got != tt.want {
				t.Errorf("Got %v but expected %v", got, tt.want)
			}
			if got2 := c.Reset().Do(tt.op).Result(); got2 != tt.want {
				t.Errorf("Object statement is not persist (got %v for %v %v)", got2, tt.a, tt.b)
			}
		})
	}
}

func main() {
	c := NewCalc()
	res := c.SetA(10).SetB(34).Do(multiply).Result()
	println(res)
}
