module gitlab.com/kochurovanatoliione/go-kata

go 1.18

require (
	github.com/brianvoe/gofakeit v3.18.0+incompatible // indirect
	github.com/gobuffalo/flect v1.0.2 // indirect
	github.com/mozillazg/go-unidecode v0.2.0 // indirect
)
